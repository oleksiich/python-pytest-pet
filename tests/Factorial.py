fact = 1


def factorial(number):
    global fact
    print("factorial result : " + " current = " + str(number) + " sum = " + str(fact))
    if number in [0, 1]: return fact
    fact = fact * number
    return factorial(number - 1)


factorial2 = lambda x: factorial2(x - 1) * x if x not in [0, 1] else x
