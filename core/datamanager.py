import pickle


def serialize():
    # имя файла, в котором мы сохраним объект
    testdatafilename = 'test.data'

    testname = ['1', '2', '3']
    # Запись в файл
    f = open(testdatafilename, 'wb')
    pickle.dump(testname, f)  # помещаем объект в файл
    f.close()
    del testname  # уничтожаем переменную shoplist
    # Считываем из хранилища
    f = open(testdatafilename, 'rb')
    storedlist = pickle.load(f)  # загружаем объект из файла
    print(storedlist)
