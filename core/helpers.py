__version__ = '0.1'


def doaction(a, b):
    print("doaction")
    aggregator = Aggregator()
    return aggregator.aggregated_action(a, b)


list = [i*2 for i in range(1,5)]

class Aggregator:
    def __init__(self, name="") -> None:
        super().__init__()
        print("__init__")
        if name == "":
            print("Name is empy")
        self.name = name

    def aggregated_action(self, a, b):
        print("aggregated_action")
        print(str(a) + ' ' + str(b))
        return self.max(a, b)

    def max(self, a, b):
        print('max')
        if a > b:
            return a
        elif a < b:
            return b
